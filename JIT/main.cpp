#include <stdio.h>
#include <stdint.h>
#include <windows.h>
#include <string>
#include "JitFunc.h"

#if _WIN64
#define IP Rip
#define AX Rax
#else
#define IP Eip
#define AX Eax
#endif

template<typename T>
void WriteInstructions(T ip, std::string instructions)
{
    memcpy((char*)ip, instructions.data(), instructions.size());
}

int WINAPI EmulationHandler(PCONTEXT context)
{
    char* ip = (char*)context->IP;
    switch ((byte)ip[1])
    {
    case 0x04: return -1;
    case 0x05: return -1;
    case 0x07: return -1;
    case 0x0A: return -1;
    case 0x0C: return -1;
    case 0x0E: return -1;
    case 0x0F: return -1;
    case 0x24: return -1;
    case 0x25: return -1;
    case 0x26: return -1;
    case 0x27: return -1;
    case 0x36: return -1;
    case 0x39: return -1;
    case 0x3B: return -1;
    case 0x3C: return -1;
    case 0x3D: return -1;
    case 0x3E: return -1;
    case 0x3F: return -1;
    case 0x7A: return -1;
    case 0x7B: return -1;
    case 0xA6: return -1;
    case 0xA7: return -1;
    case 0xFF:
        WriteInstructions(ip, { '\x05','\x38','\x05','\x00','\x00' });
        return 0;
    default:
        puts("How the hell did you actually get here!?");
        return -1;
    }
}

LONG WINAPI windows_exception_handler(EXCEPTION_POINTERS* ExceptionInfo)
{
    if(ExceptionInfo->ExceptionRecord->ExceptionCode != EXCEPTION_ILLEGAL_INSTRUCTION)
        return EXCEPTION_EXECUTE_HANDLER;
    
    if ((*(char*)ExceptionInfo->ContextRecord->IP) == 0x0F)
    {
        puts("Emulated Instruction Hit!");
        auto instructionLength = EmulationHandler(ExceptionInfo->ContextRecord);
        
        if(instructionLength == -1)
            return EXCEPTION_EXECUTE_HANDLER;

        ExceptionInfo->ContextRecord->IP += instructionLength;
        return EXCEPTION_CONTINUE_EXECUTION;
    }

    return EXCEPTION_EXECUTE_HANDLER;
}

void set_signal_handler()
{
    SetUnhandledExceptionFilter(windows_exception_handler);
}

int main(int argc, char* argv[])
{
    (void)argc;

    set_signal_handler();

    char assembly[] = {
        0xB8, 0x01, 0x00, 0x00, 0x00,
        0x0F, 0xFF, 0x90, 0x90, 0x90,
        0x90, 0x90, 0x90, 0x90, 0x90,
        0xC3 };

    JitFunc<uint32_t> func(assembly, sizeof(assembly) / sizeof(assembly[0]));
    uint32_t value = func();
    printf("ret: %d\n", value);
    value = func();
    printf("ret: %d\n", value);
    return 0;
}