#pragma once
#include <windows.h>

template<typename RET>
class JitFunc
{
    LPVOID buffer;
    RET(*function_ptr)();
public:
	JitFunc(const char* code, size_t size);
    ~JitFunc();

    RET operator()();
};

template<typename RET>
inline JitFunc<RET>::JitFunc(const char* code, size_t size)
{
    SYSTEM_INFO system_info;
    GetSystemInfo(&system_info);
    auto const page_size = system_info.dwPageSize;
    buffer = VirtualAlloc(nullptr, page_size, MEM_COMMIT, PAGE_READWRITE);
    if (buffer == nullptr) return;
    memcpy(buffer, code, size);
    DWORD dummy;
    VirtualProtect(buffer, size, PAGE_EXECUTE_READWRITE, &dummy);
    function_ptr = reinterpret_cast<RET(*)()>(buffer);
}

template<typename RET>
inline JitFunc<RET>::~JitFunc()
{
    VirtualFree(buffer, 0, MEM_RELEASE);
}

template<typename RET>
inline RET JitFunc<RET>::operator()()
{
    return function_ptr();
}
